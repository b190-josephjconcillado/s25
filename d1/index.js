console.log("Hello World!");

/**
 * SECTION - JSON OBJECTS
 *   JSON 
 *      - javascript object notation
 *      - is also used in other programming language, hence the notation in its name
 * 
 * what json does
 *  json - is used for serializing different data types into bytes
 *      bytes - is a unit of data that is composed of 8 binary digits that is used to represent a character (letters,number,typographical symbol)
 *      serialization - is the process of converting data into a series of bytes for easier transmission/transfer of information.
 *  SYNTAX:
 *      {
 *          "propertyA":"valueA",
 *          "propertyB":"valueB",
 *          "propertyC":"valueC",
 *          "propertyD":"valueD",
 *          "propertyE":"valueE"
 *      }
 */

// let cities = [
//     {city:"Ormoc City",province:"Leyte",country:"Philippines"},
//     {city:"Cebu City",province:"Leyte",country:"Philippines"},
//     {city:"Tacloban City",province:"Leyte",country:"Philippines"}];

let cities = [
    {"city":"Ormoc City","province":"Leyte","country":"Philippines"},
    {"city":"Cebu City","province":"Leyte","country":"Philippines"},
    {"city":"Tacloban City","province":"Leyte","country":"Philippines"}];
    
console.log(cities);

// JSON METHODS - json object contains method for parsing and converting data to /from json or stringified json
/**
 * stringified json is a js object converted into a string/(json format) to be used in other functions of a js application
 * they are commonly used in HTTP requests where information is required to be sent and received in a stringified json format
 * requests are an important part of programming where an application communicates with a backend application to perform different tasks such as getting/creating data in a database
 * 
 * front end application is an application that used to interact with users to perform different tasks and display information while the backend application are commonly used for all business
 * logic and data processing.
 */
let batchesArr = [{batchName: "Batch X"},{batchName:"Batch Y"}];
console.log(batchesArr);
// stringify method - used to convert js objects into a string
console.log("Result from stringify method");
console.log(JSON.stringify(batchesArr));

/*
let data = {
    name: "JC",
    age: 33,
    address: {
        city: "Cebu",
        country: "Philippines"
    }
};
console.log(JSON.stringify(data));
*/

// direct conversion of data to stringify
let data = JSON.stringify({
    name: "JC",
    age: 33,
    address: {
        city: "Cebu",
        country: "Philippines"
    }
});
console.log(data);

// an example where JSON is commonly used is on a package.json files which an  express js application uses to keep track of the information regarding a repository/project(see package.json).

// JSON.stringify with prompt()
/**
 * when information is stored in a variable and is not hard coded into an object that is being stringified, we can supply the value with variable
 */
// let fname = prompt("What is your first name?");
// let lname = prompt("What is your last name?");
// let age = prompt("What is your age?");
// let address = {
//     city: prompt("Which city do you live?"),
//     country: prompt("Which country does your city belongs?")
// };

// let otherData = JSON.stringify({
//     fname:fname,
//     lname:lname,
//     age:age,
//     address:address 
// });
// console.log(otherData);

// parse method
let batchesJSON = `[{"batchName":"Batch X"}, {"batchName":"Batch Y"}]`;
console.log(batchesJSON);
console.log("Result from parse method");
console.log(JSON.parse(batchesJSON));

console.log(JSON.parse(data));
// console.log(JSON.parse(otherData));